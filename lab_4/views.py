from django.shortcuts import render,redirect
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    response = {'collections':Note.objects.all()}
    return render(request, 'lab4_index.html', response)
 
def add_note(request):
    form = NoteForm(request.POST, request.FILES or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('/lab-4')
    else:
        form = NoteForm()
    
    return render(request, 'lab4_form.html', {'form': form})

def note_list(request):
    response = {'noteCollections':Note.objects.all()}
    return render(request, 'lab4_note_list.html', response)
