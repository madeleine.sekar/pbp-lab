from django import forms
from lab_2.models import Note
from django.utils.translation import gettext_lazy as _
from django.forms import widgets

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'fromClient', 'title','message']
        
        to = forms.CharField(max_length=30)
        fromClient = forms.CharField(max_length=30)
        title = forms.CharField(max_length=30)
        message = forms.Textarea()
        widgets ={
            'message': forms.Textarea(),
            }
        labels = {
            'to':_('TO'),
            'fromClient':_('FROM'),
            'title':_('TITLE'),
            'message':_('MESSAGE'),
            }
        
     