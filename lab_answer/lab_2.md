Apakah perbedaan antara JSON dan XML?
JSON merupakan singkatan dari Notasi Objek JavaScript yang membantu
untuk bertukar data di berbagai platform. JSON sebagian besar digunakan dengan Asynchronus JavaScript(AJAX). JSON memiliki tipe data seperti Number,Boolean, String, Array, Object, dan Null. JSON menjadi pilihan yang tepat untuk bertukar data karena memiliki ukuran yang lebih kecil sehingga pertukaran data mejadi lebih cepat. 

XML adalah singkatan dari “eXtensible Markup Language“. XML merupakan pilihan default untuk bertukar data karena hampir setiap programming language memiliki parser untuk XML. XML mempermudah pengguna untuk mengambil dan mengolah data. 

Berikut perbandingan utama dari JSON dan XML:
1. XML dirancang untuk membawa data bukan untuk menampilkan data dan JSON dirancang untuk pertukaran data yang ringan.
2. JSON berasal dari JavaScript sedangkan XML dari SGML.
3. JSON mendukung penggunaan array sedangkan XML tidak.
4. JSON tidak menggunakan tag sedangkan XML menggunakan tag.
5. JSON hanya mendukung UTF-8 encoding sedangkan XML mendukung berbagai encoding.
6. JSON lebih mudah dibaca dan dipahami sedangkan XML lebih sulit.
7. JSON berorientasi pada data sedangkan XML berorientasi pada dokumen.
8. Penyimpanan data JSON berbentuk map dengan key & value sedangkan XML berbentuk tree.




Apakah perbedaan antara HTML dan XML?
 XML memiliki sintaks berbasis tag yang sangat mirip dengan HTML, tetapi itu bukan pengganti untuk HTML. HTML memungkinkan pembuatan struktur halaman web. XML adalah tujuan yang lebih umum. Keuntungan utama XML adalah memungkinkan pemrogram membuat tag sendiri. XML adalah dasar dari banyak teknologi web.
 -XML berfokus ke transportasi data tanpa memikirkan tampilan output. Hal ini membuat XML lebih mudah digunakan karena HTML berfokus ke tampilan yang membuat instruksinya lebih kompleks.
 -XML berfokus untuk menyimpan dan mentransfer data sedangkan HTML berfokus untuk menampilkan data dan mendefinisikan struktur dari laman web. 
 -XML adalah bahasa standar yang dapat mendefinisikan bahasa komputer lain sedangkan HTML adalah bahasa yang telah ditentukan sebelumnya dengan implikasinya sendiri.
 -Dengan adanya HTML tampilan dan presentasi dari sebuah laman web dapat dimodifikasi sehingga lebih interaktif sedangkan XML hanya berfokus pada pertukaran informasi antara client dengan server.

Referensi:
https://hackr.io/blog/json-vs-xml
https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html
https://byjus.com/free-ias-prep/difference-between-xml-and-html/