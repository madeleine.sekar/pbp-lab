from django.urls import path
from .views import html, json, xml

urlpatterns = [
    path('', html, name='index'),
    path('xml/', xml, name='xml'),
    path('json/', json, name='json')
    # TODO Add friends path using friend_list Views
]
