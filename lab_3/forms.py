from django import forms
from lab_1.models import Friend
from django.utils.translation import gettext_lazy as _
from django.forms import widgets

class DateInput(forms.DateInput):
    input_type = 'date'
#https://stackoverflow.com/questions/3367091/whats-the-cleanest-simplest-to-get-running-datepicker-in-django
class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']
        name = forms.CharField(max_length=30)
        npm = forms.CharField(max_length=10)
        dob = forms.DateField()  
        widgets = {
            'dob': DateInput(),
            }
        labels = {
            'name':_('Name'),
            'npm':_('NPM'),
            'dob':_('Date of Birth'),
            }
        
     