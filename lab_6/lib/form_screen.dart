import 'package:flutter/material.dart';
//source code from https://www.youtube.com/watch?v=-6GBAGj-h4Q
// https://www.youtube.com/watch?v=nFSL-CqwRDo&t=1228s
class FormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormScreenState();
  }
}
class FormScreenState extends State<FormScreen> {
  String ? _name;
  String ? _email;
  String ? _password;
  String ? _NIK;
  String ? _Domisili;
  String ? _phoneNumber;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  Widget buildEmail(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Positioned(
          top: 0.0,
          child: Icon(Icons.email,
              size: 50, color: Colors.blueAccent[200]),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: TextFormField(
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Masukkan Email'),
              validator:(String ? value) {
                if (value == null) {
                  return 'Email is Required';
                }

                if (!RegExp(
                    r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                    .hasMatch(value)) {
                  return 'Please enter a valid email Address';
                }

                return null;
              },
            onSaved: (String ? value) {
              _email = value;
            },
          ),
        ),
      ],
    );
  }
  Widget buildNama(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Positioned(
          top: 0.0,
          child: Icon(Icons.person,
              size: 50,color: Colors.blueAccent[200]), //Icon
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: TextFormField(
            decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Masukkan Nama'),
            validator:(String ? value) {
              if (value == null) {
                return 'Name is Required';
              }

              return null;
            },
            onSaved: (String ? value) {
              _name = value;
            },
          ),
        ),
      ],
    );
  }
  Widget buildPassword(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Positioned(
          top: 0.0,
          child: Icon(Icons.lock,
              size: 50,color: Colors.blueAccent[200]), //Icon
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: TextFormField(
            decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Masukkan Password'),
            validator:(String ? value) {
              if (value==null) {
                return 'Password is Required';
              }

              return null;
            },
            onSaved: (String ? value) {
              _password = value;
            },
          ),
        ),
      ],
    );
  }
  Widget buildNIK(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Positioned(
          top: 0.0,
          child: Icon(Icons.list,
              size: 50, color:Colors.blueAccent[200]),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: TextFormField(
            decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Masukkan NIK'),
            validator: (String ? value) {
              if (value==null) {
                return 'NIK is Required';
              }

              return null;
            },
            onSaved: (String ? value) {
              _NIK = value;
            },
          ),
        ),
      ],
    );
  }
  Widget buildTelepon(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Positioned(
          top: 0.0,
          child: Icon(Icons.phone,
              size: 50, color: Colors.blueAccent[200]), //Icon
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: TextFormField(
            decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Masukkan Nama'),
            validator: (String ? value) {
              if (value== null){
                return 'Phone number is Required';
              }

              return null;
            },
            onSaved: (String ? value) {
              _phoneNumber = value;
            },
          ),
        ),
      ],
    );
  }
  Widget buildDomisili(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Positioned(
          top: 0.0,
          child: Icon(Icons.house,
              size: 50, color: Colors.blueAccent[200]), //Icon
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: TextFormField(
            decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Masukkan Domisili'),
            validator: (String ? value) {
              if (value==null){
                return 'Phone number is Required';
              }

              return null;
            },
            onSaved: (String ? value) {
              _Domisili = value;
            },
          ),
        ),
      ],
    );
  }

  String dropdownValue = 'Lokasi';
  Widget _buildDrop() {
    return DropdownButton<String>(
      value: dropdownValue,
      isExpanded:true,

      icon: const Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? newValue) {
        setState(() {
          dropdownValue = newValue!;
        });
      },
      items: <String>['RS Ceria', 'RS Sehat', 'RS Bahagia', 'Lokasi']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
  String tanggal = 'Tanggal';
  Widget dropTanggal(){
    return DropdownButton<String>(
      value: tanggal,
      isExpanded:true,

      icon: const Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String ? newValue){
        setState(() {
          tanggal= newValue!;
        });
      },
      items: <String>['Tanggal', '15 Januari 2022','20 Februari 2022','18 Maret 2022','6 Mei 2022']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
  String jenis = 'Jenis';
  Widget buildJenis(){
    return DropdownButton<String>(
      value: jenis,
      isExpanded:true,

      icon: const Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? newValue) {
        setState(() {
          jenis = newValue!;
        });
      },
      items: <String>['Jenis','SinoVac','Moderna', 'Pfizer']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
    TextButton.styleFrom(primary: Theme.of(context).colorScheme.onPrimary);    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          TextButton(
            style: style,
            onPressed: () {},
            child: const Text('Register'),
          ),
          TextButton(
            style: style,
            onPressed: () {},
            child: const Text('Login'),
          )
        ],
      ),
       body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(24),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                buildNama(),
                buildEmail(),
                buildPassword(),
                buildDomisili(),
                buildNIK(),
                buildTelepon(),
                _buildDrop(),
                dropTanggal(),
                buildJenis(),
                SizedBox(height: 100),
                ElevatedButton(
                  child: Text(
                    'Submit',
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  onPressed: () {
                   if(_formKey.currentState != null && _formKey.currentState?.validate() == false){
                     return;
                   }
                   _formKey?.currentState?.save();

                    print(_name);
                    print(_email);
                    print(_phoneNumber);
                    print(_NIK);
                    print(_password);
                    print(_Domisili);
                    print(dropdownValue);
                    print(tanggal);
                    print(jenis);
                    //Send to API
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
