import 'package:lab_6/form_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Vaksin Yuk !',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      home: FormScreen(),
    );
  }
}